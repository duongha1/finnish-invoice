package fi.vamk.studentid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void checkTheLengthGreaterThan3() {
    assertTrue(ReferenceValidator.calculateRefNumber("123").length() > 3);
    }

    @Test
    public void checkTheLengthLessThan20() {
    assertTrue(ReferenceValidator.calculateRefNumber("123").length() < 20);
    }

    @Test
    public void checkActualCheckingNumber() {
    assertEquals("2", ReferenceValidator.getActualCheckingNumber("123"));
    }
}
